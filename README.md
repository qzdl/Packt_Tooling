# Packt_Tooling
A simple tool to interact with Packt, and capitalise on the free book offering

# Configuration


# Goals
- Download free books from [Packt Pub](https://packtpub.com)
- Collect **some information** about the book
- Rename books accordingly, and export them to a directory
- Have this collection of services fully containerised

# On Modularity
When navigating the packt site, there are a few main areas to focus on to achieve the goals of this project.
- Logging in
- Free book download page
- Account page
- Book page

